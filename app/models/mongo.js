"use strict";

const mongoose = require("mongoose");
const config = require("../../config/database");

mongoose.Promise = global.Promise;
mongoose.connect(config.mongodb.url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  //   useMongoClient: true,
  //   autoIndex: false, // Don't build indexes
  //   reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  //   reconnectInterval: 500, // Reconnect every 500ms
  //   poolSize: 10, // Maintain up to 10 socket connections
  //   // If not connected, return errors immediately rather than waiting for reconnect
  //   bufferMaxEntries: 0,
});
var db = mongoose.connection;
//mongoose.set('debug', true);
db.on("error", console.error.bind(console, "MongoDB connection error:"));

db.on("open", function () {
  console.log("ready mongodb");
});

// Schema
var accounts = mongoose.Schema({}, { strict: false });
var hotlines = mongoose.Schema({}, { strict: false });
var logs = mongoose.Schema({}, { strict: false });
var customers = mongoose.Schema({}, { strict: false });
var rules = mongoose.Schema({}, { strict: false });
var tickets = mongoose.Schema({}, { strict: false });
var brandnames = mongoose.Schema({}, { strict: false });

// Model
var schema_accounts = mongoose.model("accounts", accounts);
var schema_hotlines = mongoose.model("hotlines", hotlines);
var schema_logs = mongoose.model("logs", logs);
var schema_customers = mongoose.model("customers", customers);
var schema_rules = mongoose.model("rules", rules);
var schema_tickets = mongoose.model("tickets", tickets);
var schema_brandnames = mongoose.model("brandnames", brandnames);

module.exports = {
  logs: schema_logs,
  customers: schema_customers,
  hotlines: schema_hotlines,
  accounts: schema_accounts,
  rules: schema_rules,
  tickets: schema_tickets,
  brandnames: schema_brandnames,
};
