var resp = require('../response');

module.exports = function (req, res, next) {
        const Joi = require('joi');

        const schema = Joi.object().keys({
            limit: Joi.number().min(0).max(500),
            offset: Joi.number().min(0),
            fields: Joi.string().regex(/^[a-zA-Z0-9/,/_]{2,30}$/),
            desc: Joi.string().regex(/^[a-zA-Z0-9/,/_]{2,30}$/),
            asc: Joi.string().regex(/^[a-zA-Z0-9/,/_]{2,30}$/)
        });

        console.log(req.originalUrl,req.method);

        // const result = Joi.validate(req.query, schema,{allowUnknown:true});

        const result = schema.validate(req.query, { allowUnknown: true });

        if(result.error){
            resp.babRequest(res,req,result.error.message);
        }else {
            next();
        }

}
