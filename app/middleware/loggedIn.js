var resp = require("../response");
var jwt = require("jsonwebtoken");
var config = require("../../config/jwt");
var db = require("../../config/database");
var models = require("../models");
var crypto = require("crypto");

var jwtConf = require("../../config/jwt")

module.exports = async function isLoggedIn(req, res, next) {
  let token = req.header("authorization");
  try {
    if (token) {
      token = token.split(" ")[1];
      let decoded = null;
      try {
        decoded = jwt.verify(token, config.secret_token);
        if(decoded.typeLogin == 1){
          const account = await models.mongo.accounts.findOne({
            Code: decoded.Code,
          });
  
          
          if (account.Deleted) {
            resp.forbidden(req, res, { message: "account đã bị xóa" });
          }
          req.user = {
            Code: account.Code,
            Role: account.Role,
            typeLogin: account.typeLogin,
            Deleted: account.Deleted,
            FullName: account.FullName,
            Email: account.Email,
            PhoneNumber: account.PhoneNumber,
            Role: account.Role
          };
        }

        if(decoded.typeLogin == 2){
          const account = await models.mongo.customers.findOne({enterprise_number: decoded.enterprise_number})
          req.user = {
            enterprise_number: account.enterprise_number,
            companyname: account.companyname,
            typeLogin: account.typeLogin,
            Role: jwtConf.roles.CUSTOMER
          };
        }
    
        // req.log = {
        //   activity: crypto
        //     .createHash("md5")
        //     .update(Date.now().toString() + Math.random())
        //     .digest("hex")
        //     .substring(0, 24),
        // };
        next();
      } catch (err) {
        resp.forbidden(res, req, { message: err.message });
      }
    } else {
      next();
    }
  } catch (err) {
    resp.throws(res, req, err);
  }
};
