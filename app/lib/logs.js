let models = require("../models");
const jwtConf = require("../../config/jwt");
const Joi = require("joi");
const Promisebb = require("bluebird");

// {
//   Type,
//   Hotline_code,
//   Hotline_info,
//   Tenant_code,
//   Update_key,
//   Code,
//   Tenant_role,
// }

exports.create = async (data) => {
  console.log(data, "logggggggggggggggggggggggggggggggg");
  let payload = data.map((value) => {
    (value.Created_at = new Date()), (value.Updated_at = new Date());
    return value;
  });
  // let logs = {
  //   Type,
  //   Hotline_code,
  //   Hotline_info,
  //   Tenant_code,
  //   Update_key,
  //   Code,
  //   Tenant_role,
  //   Created_at: new Date(),
  //   Updated_at: new Date(),
  // };

  const logsres = await models.mongo.logs.insertMany(payload);

  if (logsres) {
    return Promise.resolve(logsres);
  } else
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, xin vui lòng thử lại",
    });
};

exports.list = async (user, options) => {
  let {
    limit,
    offset,
    sort_by,
    Hotline_code,
    from_date,
    to_date,
    Type,
    Hotline,
  } = options;
  let schema = Joi.object().keys({
    offset: Joi.number().integer(),
    limit: Joi.number().integer(),
    sort_by: Joi.string(),
    Hotline_code: Joi.string(),
    Hotline: Joi.string(),
  });

  let result = schema.validate(options, { allowUnknown: true });
  if (user.Role == jwtConf.roles.CUSTOMER) {
    return Promise.reject({
      show: true,
      message: "Không có quyền xem lịch sử tác động",
    });
  }
  if (result.error) {
    return Promise.reject({ show: true, message: result.error.message });
  }
  let condition = {};

  console.log(Hotline, "Hotline");
  if (Hotline) {
    let Hotline_info = await models.mongo.hotlines.findOne({ Hotline });
    console.log(Hotline_info, "Hotline_info");
    condition["Hotline_code"] = Hotline_info.Code;
  }

  // if (Hotline_code) {
  //   condition["Hotline_code"] = Hotline_code;
  // }

  if (Type) {
    condition["Type"] = Type;
  }

  let query = models.mongo.logs.find(condition);
  let count = models.mongo.logs.countDocuments(condition);

  if (from_date) {
    // from = moment(from_date, "YYYY-MM-DD HH:mm").toDate();
    const fromDate = new Date(from_date);
    query.where("Created_at").gt(fromDate);
    count.where("Created_at").gt(fromDate);
  }

  if (to_date) {
    // to = moment(to_date, "YYYY-MM-DD HH:mm").toDate();
    const toDate = new Date(to_date);
    query.where("Created_at").lt(toDate);
    count.where("Created_at").lt(toDate);
  }

  if (sort_by) {
    let sort = sort_by.split(":");
    let sort_obj = {};
    sort_obj[sort[0]] = sort[1];
    query.sort(sort_obj);
  } else {
    query.sort({ Created_at: -1 });
  }

  if (limit) {
    query.limit(parseInt(limit));
  } else {
    query.limit(10);
  }
  if (offset) {
    query.skip(parseInt(offset));
  } else {
    query.skip(0);
  }

  let datas = await Promisebb.props({
    data: query,
    total: count,
  });

  let newdata = [];
  for (let i = 0; i < datas.data.length; i++) {
    let value = JSON.parse(JSON.stringify(datas.data[i]));
    if (datas.data[i].Tenant_code == jwtConf.roles.CUSTOMER) {
      let Tenant_info_unSip = await models.mongo.accounts.findOne({
        enterprise_number: datas.data[i].Tenant_code,
      });
      if (Tenant_info_unSip) {
        value.Tenant_info = Tenant_info_unSip;
      } else {
        let Tenant_info_unSip = await models.mongo.customers.findOne({
          enterprise_number: datas.data[i].Tenant_code,
        });
        value.Tenant_info = Tenant_info_unSip;
      }
    }

    if (datas.data[i].Tenant_code !== jwtConf.roles.CUSTOMER) {
      let Tenant_info = await models.mongo.accounts.findOne({
        Code: datas.data[i].Tenant_code,
      });
      if (Tenant_info) {
        value.Tenant_info = Tenant_info;
      }
    }
    newdata.push(value);
  }

  return Promise.resolve({
    list: newdata,
    total: datas.total,
  });
};
