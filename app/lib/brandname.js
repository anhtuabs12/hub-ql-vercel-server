let models = require("../models");
const jwtConf = require("../../config/jwt");
const randomId = require("random-id");
const logs = require("./logs");
const Joi = require("joi");
const Promisebb = require("bluebird");
const brandname = require("../routes/brandname");
const { forEach } = require("lodash");

exports.create = async (user, body = {}) => {
  let schema = Joi.object().keys({
    Certificate_number: Joi.string().required(),
    Enterprise_number_company: Joi.string().required(),
    Brandname: Joi.string().required(),
    Status: Joi.string().required(),
    Expiry_at: Joi.string(),
    Lunch_date: Joi.string(),
    Note: Joi.string(),
  });

  if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
    return Promise.reject({
      show: true,
      message: "Không có quền thêm tạo brandname",
    });
  } else {
    let result = schema.validate(body, { allowUnknown: true });
    if (result.error) {
      return Promise.reject({ show: true, message: result.error.message });
    }
    body.Note = body.Note.trim();

    let brandnames = {
      Code: randomId(12, "A0"),
      Created_at: new Date(),
      Updated_at: new Date(),
      Certificate_number: body.Certificate_number || null,
      Enterprise_number_company: body.Enterprise_number_company || null,
      Brandname: body.Brandname || null,
      Status: body.Status || null,
      Expiry_date: body.Expiry_date,
      Lunch_date: body.Lunch_date,
      Note: body.Note,
      Deleted: false,
    };

    let brandnameres = await models.mongo.brandnames(brandnames).save();
    if (brandnameres) {
      return Promise.resolve(brandnameres);
    } else {
      return Promise.reject({
        show: true,
        message: "Xảy ra lỗi, xin vui lòng thử lại",
      });
    }
  }
};

exports.update = async (brandname_code, user, body = {}) => {
  if (brandname_code) {
    let checkbrandname = await models.mongo.brandnames.findOne({
      code: body.brandname_code,
    });

    if (!checkbrandname) {
      return Promise.reject({
        show: true,
        message: "Brandname không tồn tại",
      });
    }

    let checkbrandnameHotline = await models.mongo.hotlines.findOne({
      BrandName: brandname_code,
    });

    if (checkbrandnameHotline) {
      return Promise.reject({
        show: true,
        message: "Brandname đã được sử dụng",
      });
    }

    switch (user.Role) {
      case jwtConf.roles.ADMINISTRATOR:
        let brandnames = {
          ...body,
          Updated_at: new Date(),
        };

        const randnameres = await models.mongo.brandnames.updateOne(
          { Code: brandname_code },
          brandnames,
          { new: true }
        );
        if (randnameres) {
          return Promise.resolve(randnameres);
        } else {
          return Promise.reject({
            show: true,
            message: "Xảy ra lỗi, xin vui lòng thử lại",
          });
        }

      default:
        break;
    }
  } else {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, xin vui lòng thử lại",
    });
  }
};

exports.delete = async (brandname_code, user) => {
  if (brandname_code) {
    let checkbrandname = await models.mongo.brandnames.findOne({
      Code: brandname_code,
    });

    if (!checkbrandname) {
      return Promise.reject({
        show: true,
        message: "Brandname không tồn tại",
      });
    }

    let brandnames = {
      Deleted: true,
      Updated_at: new Date(),
    };
    const brandnameres = await models.mongo.brandnames.updateOne(
      { Code: brandname_code },
      brandnames,
      { new: true }
    );
    if (brandnameres) {
      return Promise.resolve(brandnameres);
    } else {
      return Promise.reject({
        show: true,
        message: "Xảy ra lỗi, xin vui lòng thử lại",
      });
    }
  } else {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, xin vui lòng thử lại",
    });
  }
};

exports.list = async (user, body = {}) => {
  let {
    Expiry_form_date,
    Expiry_to_date,
    Lunch_form_date,
    Lunch_to_date,
    Hotline_num,
    Brandname,
    Brandname_Status,
    Number_company,
    Certificate_number,
    limit,
    offset,
    sort_by,
    from_date,
    to_date,
  } = body;

  let schema = Joi.object().keys({
    offset: Joi.number().integer(),
    limit: Joi.number().integer(),
    sort_by: Joi.string(),
    from: Joi.string(),
    to: Joi.string(),
    Certificate_number: Joi.string(),
    Number_company: Joi.string(),
    Brandname_Status: Joi.array(),
    Brandname: Joi.string(),
    Hotline_num: Joi.number(),
  });

  let result = schema.validate(body, { allowUnknown: true });
  if (result.error) {
    return Promise.reject({ show: true, message: result.error.message });
  }
  let condition = { Deleted: false };
  if (Certificate_number) {
    condition["Certificate_number"] = {
      $regex: body.Certificate_number.toLowerCase(),
    };
  }
  if (Number_company) {
    condition["Enterprise_number_company"] = {
      $regex: body.Number_company.toLowerCase(),
    };
  }
  if (Brandname_Status?.length > 0) {
    condition["Status"] = { $in: Brandname_Status };
  }
  if (Brandname) {
    condition["Brandname"] = { $regex: body.Brandname.toLowerCase() };
  }

  if (user.Role == jwtConf.roles.SALEADMIN) {
    return Promise.reject({
      show: true,
      message: "Không có quền xem",
    });
  }

  let query = models.mongo.brandnames.find(condition);
  let count = models.mongo.brandnames.countDocuments(condition);

  if (from_date) {
    // from = moment(from_date, "YYYY-MM-DD HH:mm").toDate();
    const fromDate = new Date(from_date);
    query.where("Created_at").gt(fromDate);
    count.where("Created_at").gt(fromDate);
  }

  if (to_date) {
    // to = moment(to_date, "YYYY-MM-DD HH:mm").toDate();
    const toDate = new Date(to_date);
    query.where("Created_at").lt(toDate);
    count.where("Created_at").lt(toDate);
  }

  if (Lunch_form_date) {
    // from = moment(from_date, "YYYY-MM-DD HH:mm").toDate();
    // const fromDate = new Date(Lunch_form_date);
    query.where("Lunch_date").gt(Lunch_form_date);
    count.where("Lunch_date").gt(Lunch_form_date);
  }

  if (Lunch_to_date) {
    // to = moment(to_date, "YYYY-MM-DD HH:mm").toDate();
    // const toDate = new Date(Lunch_to_date);
    query.where("Lunch_date").lt(Lunch_to_date);
    count.where("Lunch_date").lt(Lunch_to_date);
  }

  if (Expiry_form_date) {
    // from = moment(from_date, "YYYY-MM-DD HH:mm").toDate();
    const fromDate = new Date(Expiry_form_date);
    query.where("Expiry_date").gt(fromDate);
    count.where("Expiry_date").gt(fromDate);
  }

  if (Expiry_to_date) {
    // to = moment(to_date, "YYYY-MM-DD HH:mm").toDate();
    const toDate = new Date(Expiry_to_date);
    query.where("Expiry_date").lt(toDate);
    count.where("Expiry_date").lt(toDate);
  }

  if (sort_by) {
    let sort = sort_by.split(":");
    let sort_obj = {};
    sort_obj[sort[0]] = sort[1];
    query.sort(sort_obj);
  } else {
    query.sort({ Created_at: -1 });
  }

  if (limit) {
    query.limit(parseInt(limit));
  }
  if (offset) {
    query.skip(parseInt(offset));
  } else {
    query.skip(0);
  }

  let datas = await Promisebb.props({
    data: query,
    total: count,
  });
  let newdata = [];
  for (let i = 0; i < datas.data.length; i++) {
    const checkBrandnameToHotline = await models.mongo.hotlines.countDocuments({
      BrandName: datas.data[i].Code,
    });
    let value = JSON.parse(JSON.stringify(datas.data[i]));
    value.hotlineTotal = checkBrandnameToHotline;
    newdata.push(value);
  }

  return Promise.resolve({
    list: newdata,
    total: datas.total,
  });
};
exports.findbycode = async (brandname_code, user) => {
  const checkBrandname = await models.mongo.brandnames.findOne({
    Code: brandname_code,
  });
  const checkBrandnameToHotline = await models.mongo.hotlines.find({
    BrandName: brandname_code,
  });
  const brandname = {
    checkBrandname,
    checkBrandnameToHotline,
  };

  if (!checkBrandname) {
    return Promise.reject({
      show: true,
      message: "Brandname không tồn tại",
    });
  } else {
    return Promise.resolve(brandname);
  }
};
