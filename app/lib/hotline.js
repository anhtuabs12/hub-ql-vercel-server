let models = require("../models");
const Joi = require("joi");
const _ = require("lodash");
const jwtConf = require("../../config/jwt");
const logs = require("./logs");
const randomId = require("random-id");
const Promisebb = require("bluebird");

exports.create = async (user, body = {}, io) => {
  let schema = Joi.object().keys({
    Type_NetWork: Joi.string().required(),
    Hotline: Joi.string().required(),
    Status: Joi.string().required(),
    CustomerCode: Joi.string(),
    Lunch_date: Joi.string(),
    BrandName: Joi.string(),
    PurchaseDate: Joi.string(),
    Customer_end: Joi.string(),
    Extention_periodDate: Joi.string(),
  });

  if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
    return Promise.reject({
      show: true,
      message: "Không có quyền add số vào kho",
    });
  }

  let errs = schema.validate(body, { allowUnknown: true });
  let err = errs.err;
  if (err) return Promise.reject({ show: true, message: err.message });

  let hotlineList = body.Hotline.split(",");

  hotlineList.map(async (value) => {
    if (value) {
      let newHotline = value ? value.toLowerCase().trim() : "";
      if (value) {
        let checkExists = await models.mongo.hotlines.findOne({
          Hotline: value,
          Type_NetWork: body.Type_NetWork,
        });
        if (checkExists) {
          return Promise.reject({
            show: true,
            message: "Hotline đã tồn tại",
          });
        }
      }

      let hotlines = {
        Extention_periodDate: body.Extention_periodDate || null,
        Code: randomId(12, "A0"),
        Type_NetWork: body.Type_NetWork, //id nhà mạng
        Hotline: newHotline, // hotline
        PurchaseDate: body.PurchaseDate, //ngày mua
        CustomerCode: body.CustomerCode || null, // code khách hàng
        Lunch_date: body.Lunch_date || null, // ngày cấp hotline cho khách hàng
        Status: body.Status || null,
        BrandName: body.BrandName || null,
        Created_at: new Date(),
        Updated_at: new Date(),
        Customer_end: body.Customer_end,
        Tanent_code: user.Code,
      };

      let hotline = await models.mongo.hotlines(hotlines).save();
      if (hotline) {
        io.emit(jwtConf.socketType.REQEST_RELOAD_HOTLINE, { status: true });
        let log = [
          {
            BrandLogs: jwtConf.BrandLogsType.HOTLINE,
            Code: randomId(12, "A0"),
            Type: jwtConf.logType.CREATE,
            Hotline_code: hotline.Code,
            Tenant_code: user.Code,
            Hotline_info: hotline,
            Update_key: [],
            Tenant_role: user.Role,
          },
        ];
        await logs.create(log);
        return Promise.resolve(hotline);
      } else
        return Promise.reject({
          show: true,
          message: "Xảy ra lỗi, xin vui lòng thử lại",
        });
    }
  });
};

exports.update = async (hotline_code, user, body, io) => {
  if (hotline_code) {
    let checkhotline = await models.mongo.hotlines.findOne({
      Code: hotline_code,
    });
    if (!checkhotline) {
      return Promise.reject({
        show: true,
        message: "Hotline không tồn tại",
      });
    }

    if (user.Role == jwtConf.roles.ADMINISTRATOR) {
      // return Promise.reject({
      //   show: true,
      //   message: "Không có quyền cập nhật số vào kho",
      // });
    }

    switch (user.Role) {
      case jwtConf.roles.ADMINISTRATOR:
        let hotline = {
          ...body,
          Updated_at: new Date(),
        };

        if (
          body.Status == jwtConf.hotlineStatus.READY ||
          body.Status == jwtConf.hotlineStatus.KEEPING ||
          body.Status == jwtConf.hotlineStatus.OFF ||
          body.Status == jwtConf.hotlineStatus.EVICT
        ) {
          (hotline.BrandName = null), (hotline.Lunch_date = null);
        }

        if (
          body.Status == jwtConf.hotlineStatus.READY ||
          body.Status == jwtConf.hotlineStatus.OFF ||
          body.Status == jwtConf.hotlineStatus.EVICT
        ) {
          console.log(123123);
          hotline.CustomerCode = null;
        }

        console.log(hotline, "hotline")

        function compareObjects(objA, objB) {
          const keysA = Object.keys(objA);
          const keysB = Object.keys(objB);

          const uniqueToA = keysA.filter((key) => !keysB.includes(key));
          const uniqueToB = keysB.filter((key) => !keysA.includes(key));

          const differentValues = keysA.filter(
            (key) => keysB.includes(key) && objA[key] !== objB[key]
          );

          return {
            uniqueToA,
            uniqueToB,
            differentValues,
          };
        }

        let checkkeyChange = compareObjects(checkhotline, hotline);

        const updatehotline = await models.mongo.hotlines.updateOne(
          { Code: hotline_code },
          hotline
        );

        if (updatehotline) {
          let data = [
            {
              BrandLogs: jwtConf.BrandLogsType.HOTLINE,
              Code: randomId(12, "A0"),
              Type: jwtConf.logType.UPDATE,
              Hotline_code: checkhotline.Code,
              Tenant_code: user.Code,
              Hotline_info: checkhotline,
              Update_key: [...checkkeyChange.differentValues].filter(
                (value) => value !== "Updated_at"
              ),
              Tenant_role: user.Role,
            },
          ];
          await logs.create(data);
          return Promise.resolve(updatehotline);
        } else
          return Promise.reject({
            show: true,
            message: "Xảy ra lỗi, xin vui lòng thử lại",
          });

      case jwtConf.roles.CUSTOMER:
        let hotlineCs = {
          Status: jwtConf.hotlineStatus.KEEPING,
          Updated_at: new Date(),
          Upadtedby: user.enterprise_number,
        };

        const updatehotlinecs = await models.mongo.hotlines.updateOne(
          { Code: hotline_code },
          hotlineCs
        );

        if (updatehotlinecs) {
          return Promise.resolve(updatehotlinecs);
        } else
          return Promise.reject({
            show: true,
            message: "Xảy ra lỗi, xin vui lòng thử lại",
          });

      default:
        break;
    }
  } else {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, xin vui lòng thử lại",
    });
  }
};

exports.updatemany = async (hotline_codes, user, body) => {
  if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
    return Promise.reject({
      show: true,
      message: "Không có quyền add số vào kho",
    });
  }

  if (hotline_codes?.length > 0) {
    // let checkhotline = await models.mongo.hotlines.findOne({
    //   code: body.hotline_code,
    // });
    // if (!checkhotline) {
    //   return Promise.reject({
    //     show: true,
    //     message: "Hotline không tồn tại",
    //   });
    // }

    let hotline = {
      Updated_at: new Date(),
    };

    if (body.Status) {
      if (
        body.Status == jwtConf.hotlineStatus.EVICT ||
        body.Status == jwtConf.hotlineStatus.OFF
      ) {
        hotline.CustomerCode = null;
        hotline.Lunch_date = null;
      }
      hotline.Status = body.Status;
    }

    if (body.Type_NetWork) {
      hotline.Type_NetWork = body.Type_NetWork;
    }

    if (body.CustomerCode) {
      hotline.CustomerCode = body.CustomerCode;
    }

    const updatehotline = await models.mongo.hotlines.updateMany(
      { Code: { $in: hotline_codes } },
      hotline
    );

    if (updatehotline) {
      // let log = {
      //   code: randomId(6, "A0"),
      //   action: jwtConf.actions.UPDATED_HOTLINE,
      //   type: jwtConf.typelogs.HOTLINE,
      //   account_impact: user,
      //   imports: false,
      //   newHotline: hotline,
      //   newAccount: null,
      //   newCustomer: null,
      // };
      // await logs.create(log);
      return Promise.resolve(updatehotline);
    } else
      return Promise.reject({
        show: true,
        message: "Xảy ra lỗi, xin vui lòng thử lại",
      });
  } else {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, xin vui lòng thử lại",
    });
  }
};

exports.delete = async (hotline_code, user) => {
  if (hotline_code) {
    let checkhotline = await models.mongo.hotlines.findOne({
      code: hotline_code,
    });
    if (!checkhotline) {
      return Promise.reject({
        show: true,
        message: "Hotline không tồn tại",
      });
    }

    let hotline = {
      ...checkhotline,
      deleted: true,
      updated_at: new Date(),
    };

    const deletedhotline = await models.mongo.hotlines.update(
      { code: hotline_code },
      hotline
    );

    if (deletedhotline) {
      let log = {
        code: randomId(6, "A0"),
        action: jwtConf.actions.DELETE_HOTLINE,
        type: jwtConf.typelogs.HOTLINE,
        account_impact: user,
        imports: false,
        newHotline: hotline,
        newAccount: null,
        newCustomer: null,
      };
      await logs.create(log);
      return Promise.resolve(deletedhotline);
    } else
      return Promise.reject({
        show: true,
        message: "Xảy ra lỗi, xin vui lòng thử lại",
      });
  } else {
    return Promise.reject({
      show: true,
      message: "Lỗi",
    });
  }
};

exports.findbycode = async (hotline_code, user) => {
  const checkhotline = await models.mongo.hotlines.findOne({
    Code: hotline_code,
  });
  if (!checkhotline) {
    return Promise.reject({
      show: true,
      message: "Hotline không tồn tại",
    });
  } else {
    return Promise.resolve(checkhotline);
  }
};

exports.keeping = async (user, body, io) => {
  let { hotline_codes, Extention_periodDate } = body;

  if (!hotline_codes || hotline_codes.length == 0) {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, số không hợp lệ",
    });
  }

  const checkcount = await models.mongo.hotlines.find({
    Code: { $in: hotline_codes },
  });
  if (checkcount.length !== hotline_codes.length) {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, số không hợp lệ",
    });
  }

  switch (user.Role) {
    case jwtConf.roles.CUSTOMER:
      const ruleblockAmount = await models.mongo.rules.findOne({
        Type: jwtConf.ruleType.MINIMUM_NUMBER_HOTLINES,
      });
      const checkAmoutkeep = await models.mongo.hotlines.find({
        Status: jwtConf.hotlineStatus.KEEPING,
        CustomerCode: user.enterprise_number,
      });
      if (hotline_codes.length > Number(ruleblockAmount.Value)) {
        return Promise.reject({
          show: true,
          message: "Quá số lượng cho phép",
        });
      }

      if (
        hotline_codes.length + checkAmoutkeep.length >
        Number(ruleblockAmount.Value)
      ) {
        return Promise.reject({
          show: true,
          message: "Quá số lượng cho phép",
        });
      }

      let currentDate = new Date();
      let hotlineCs = {
        Extention_periodDate: currentDate.setHours(currentDate.getHours() + 12),
        CustomerCode: user.enterprise_number,
        Status: jwtConf.hotlineStatus.KEEPING,
        Updated_at: new Date(),
        Upadtedby: user.enterprise_number,
      };

      const updatehotline = await models.mongo.hotlines.updateMany(
        { Code: { $in: hotline_codes } },
        hotlineCs
      );

      if (updatehotline) {
        let hotlineUpdates = await models.mongo.hotlines.find({
          Code: { $in: hotline_codes },
        });
        io.emit(jwtConf.socketType.REQEST_RELOAD_HOTLINE, { status: true });
        let dataLog = hotlineUpdates.map((value) => {
          let data = {
            BrandLogs: jwtConf.BrandLogsType.HOTLINE,
            Code: randomId(12, "A0"),
            Type: jwtConf.logType.UPDATE,
            Hotline_code: value.Code,
            Tenant_code: user.enterprise_number,
            Hotline_info: value,
            Update_key: [jwtConf.logTypeKey.STATUS_KEEPING],
            Tenant_role: user.Role,
          };
          return data;
        });
        let checklog = await logs.create(dataLog);
        console.log(
          checklog,
          "checklog+++++++++++++++++++++++++++++++++++++++++++++"
        );
        return Promise.resolve(updatehotline);
      } else
        return Promise.reject({
          show: true,
          message: "Xảy ra lỗi, xin vui lòng thử lại",
        });

    case jwtConf.roles.ADMINISTRATOR:
    case jwtConf.roles.SALEADMIN:
      let hotlineCs2 = {
        Extention_periodDate: Extention_periodDate,
        CustomerCode: body.CustomerCode,
        Status: jwtConf.hotlineStatus.KEEPING,
        Updated_at: new Date(),
        Upadtedby: user.Code,
      };

      const updatehotline2 = await models.mongo.hotlines.updateMany(
        { Code: { $in: hotline_codes } },
        hotlineCs2
      );

      if (updatehotline2) {
        io.emit(jwtConf.socketType.REQEST_RELOAD_HOTLINE, { status: true });
        let hotlineUpdates = await models.mongo.hotlines.find({
          Code: { $in: hotline_codes },
        });
        let dataLog = hotlineUpdates.map((value) => {
          let data = {
            BrandLogs: jwtConf.BrandLogsType.HOTLINE,
            Code: randomId(12, "A0"),
            Type: jwtConf.logType.UPDATE,
            Hotline_code: value.Code,
            Tenant_code: user.Code,
            Hotline_info: value,
            Update_key: [jwtConf.logTypeKey.STATUS_KEEPING],
            Tenant_role: user.Role,
          };
          return data;
        });
        await logs.create(dataLog);
        return Promise.resolve(updatehotline2);
      } else
        return Promise.reject({
          show: true,
          message: "Xảy ra lỗi, xin vui lòng thử lại",
        });
    default:
      break;
  }
};

exports.evicting = async (user, body, io) => {
  let { hotline_codes } = body;

  if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
    return Promise.reject({
      show: true,
      message: "Không có quyền cập nhật trạng thái thu hồi số",
    });
  }

  if (!hotline_codes || hotline_codes.length == 0) {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, số không hợp lệ",
    });
  }

  const checkcount = await models.mongo.hotlines.find({
    Code: { $in: hotline_codes },
  });
  if (checkcount.length !== hotline_codes.length) {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, số không hợp lệ",
    });
  }

  switch (user.Role) {
    case jwtConf.roles.ADMINISTRATOR:
      let hotlineCs = {
        CustomerCode: null,
        BrandName: null,
        Status: jwtConf.hotlineStatus.EVICT,
        Lunch_date: null,
        Updated_at: new Date(),
        Upadtedby: user.Code,
        Extention_periodDate: null,
      };

      const updatehotline = await models.mongo.hotlines.updateMany(
        { Code: { $in: hotline_codes } },
        hotlineCs
      );

      if (updatehotline) {
        io.emit(jwtConf.socketType.REQEST_RELOAD_HOTLINE, { status: true });
        let hotlineUpdates = await models.mongo.hotlines.find({
          Code: { $in: hotline_codes },
        });
        let dataLog = hotlineUpdates.map((value) => {
          let data = {
            BrandLogs: jwtConf.BrandLogsType.HOTLINE,
            Code: randomId(12, "A0"),
            Type: jwtConf.logType.UPDATE,
            Hotline_code: value.Code,
            Tenant_code: user.Code,
            Hotline_info: value,
            Update_key: [jwtConf.logTypeKey.STATUS_EVICTING],
            Tenant_role: user.Role,
          };
          return data;
        });
        await logs.create(dataLog);
        return Promise.resolve(updatehotline);
      } else
        return Promise.reject({
          show: true,
          message: "Xảy ra lỗi, xin vui lòng thử lại",
        });
    default:
      break;
  }
};

exports.canceling = async (user, body, io) => {
  let { hotline_codes } = body;

  if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
    return Promise.reject({
      show: true,
      message: "Không có quyền cập nhật trạng thái thu hồi số",
    });
  }

  if (!hotline_codes || hotline_codes.length == 0) {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, số không hợp lệ",
    });
  }

  const checkcount = await models.mongo.hotlines.find({
    Code: { $in: hotline_codes },
  });
  if (checkcount.length !== hotline_codes.length) {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, số không hợp lệ",
    });
  }

  switch (user.Role) {
    case jwtConf.roles.ADMINISTRATOR:
      let hotlineCs = {
        Extention_periodDate: null,
        CustomerCode: null,
        BrandName: null,
        Status: jwtConf.hotlineStatus.OFF,
        Lunch_date: null,
        Updated_at: new Date(),
        Upadtedby: user.Code,
      };

      const updatehotline = await models.mongo.hotlines.updateMany(
        { Code: { $in: hotline_codes } },
        hotlineCs
      );

      if (updatehotline) {
        io.emit(jwtConf.socketType.REQEST_RELOAD_HOTLINE, { status: true });
        let hotlineUpdates = await models.mongo.hotlines.find({
          Code: { $in: hotline_codes },
        });
        let dataLog = hotlineUpdates.map((value) => {
          let data = {
            BrandLogs: jwtConf.BrandLogsType.HOTLINE,
            Code: randomId(12, "A0"),
            Type: jwtConf.logType.UPDATE,
            Hotline_code: value.Code,
            Tenant_code: user.Code,
            Hotline_info: value,
            Update_key: [jwtConf.logTypeKey.STATUS_CANCELING],
            Tenant_role: user.Role,
          };
          return data;
        });
        await logs.create(dataLog);
        return Promise.resolve(updatehotline);
      } else
        return Promise.reject({
          show: true,
          message: "Xảy ra lỗi, xin vui lòng thử lại",
        });
    default:
      break;
  }
};

exports.ready = async (user, body, io) => {
  let { hotline_codes } = body;

  if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
    return Promise.reject({
      show: true,
      message: "Không có quyền cập nhật trạng thái thu hồi số",
    });
  }

  if (!hotline_codes || hotline_codes.length == 0) {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, số không hợp lệ",
    });
  }

  const checkcount = await models.mongo.hotlines.find({
    Code: { $in: hotline_codes },
  });
  if (checkcount.length !== hotline_codes.length) {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, số không hợp lệ",
    });
  }

  switch (user.Role) {
    case jwtConf.roles.ADMINISTRATOR:
      let hotlineCs = {
        Extention_periodDate: null,
        CustomerCode: null,
        Status: jwtConf.hotlineStatus.READY,
        Extention_periodDate: null,
        BrandName: null,
        Lunch_date: null,
        Updated_at: new Date(),
        Upadtedby: user.Code,
      };

      const updatehotline = await models.mongo.hotlines.updateMany(
        { Code: { $in: hotline_codes } },
        hotlineCs
      );

      if (updatehotline) {
        io.emit(jwtConf.socketType.REQEST_RELOAD_HOTLINE, { status: true });
        let hotlineUpdates = await models.mongo.hotlines.find({
          Code: { $in: hotline_codes },
        });
        let dataLog = hotlineUpdates.map((value) => {
          let data = {
            BrandLogs: jwtConf.BrandLogsType.HOTLINE,
            Code: randomId(12, "A0"),
            Type: jwtConf.logType.UPDATE,
            Hotline_code: value.Code,
            Tenant_code: user.Code,
            Hotline_info: value,
            Update_key: [jwtConf.logTypeKey.STATUS_READY],
            Tenant_role: user.Role,
          };
          return data;
        });
        await logs.create(dataLog);
        return Promise.resolve(updatehotline);
      } else
        return Promise.reject({
          show: true,
          message: "Xảy ra lỗi, xin vui lòng thử lại",
        });
    default:
      break;
  }
};

exports.doing = async (user, body, io) => {
  let { hotline_codes, CustomerCode, Lunch_date, BrandName } = body;

  if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
    return Promise.reject({
      show: true,
      message: "Không có quyền cập nhật trạng thái thu hồi số",
    });
  }

  if (!hotline_codes || hotline_codes.length == 0) {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, số không hợp lệ",
    });
  }

  const checkcount = await models.mongo.hotlines.find({
    Code: { $in: hotline_codes },
  });
  if (checkcount.length !== hotline_codes.length) {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, số không hợp lệ",
    });
  }

  switch (user.Role) {
    case jwtConf.roles.ADMINISTRATOR:
      let hotlineCs = {
        BrandName: BrandName,
        Extention_periodDate: null,
        CustomerCode: CustomerCode,
        Status: jwtConf.hotlineStatus.DOING,
        Lunch_date: Lunch_date,
        Updated_at: new Date(),
        Upadtedby: user.Code,
      };

      const updatehotline = await models.mongo.hotlines.updateMany(
        { Code: { $in: hotline_codes } },
        hotlineCs
      );

      if (updatehotline) {
        io.emit(jwtConf.socketType.REQEST_RELOAD_HOTLINE, { status: true });
        let hotlineUpdates = await models.mongo.hotlines.find({
          Code: { $in: hotline_codes },
        });
        let dataLog = hotlineUpdates.map((value) => {
          let data = {
            BrandLogs: jwtConf.BrandLogsType.HOTLINE,
            Code: randomId(12, "A0"),
            Type: jwtConf.logType.UPDATE,
            Hotline_code: value.Code,
            Tenant_code: user.Code,
            Hotline_info: value,
            Update_key: [jwtConf.logTypeKey.STATUS_DOING],
            Tenant_role: user.Role,
          };
          return data;
        });
        await logs.create(dataLog);
        return Promise.resolve(updatehotline);
      } else
        return Promise.reject({
          show: true,
          message: "Xảy ra lỗi, xin vui lòng thử lại",
        });
    default:
      break;
  }
};

exports.waitingbrandname = async (user, body, io) => {
  let { hotline_codes, CustomerCode, BrandName } = body;

  if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
    return Promise.reject({
      show: true,
      message: "Không có quyền cập nhật trạng thái thu hồi số",
    });
  }

  if (!hotline_codes || hotline_codes.length == 0) {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, số không hợp lệ",
    });
  }

  const checkcount = await models.mongo.hotlines.find({
    Code: { $in: hotline_codes },
  });
  if (checkcount.length !== hotline_codes.length) {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, số không hợp lệ",
    });
  }

  switch (user.Role) {
    case jwtConf.roles.ADMINISTRATOR:
      let hotlineCs = {
        BrandName: BrandName,
        Extention_periodDate: null,
        CustomerCode: CustomerCode,
        Status: jwtConf.hotlineStatus.WAITBRANDNAME,
        Lunch_date: null,
        Updated_at: new Date(),
        Upadtedby: user.Code,
      };

      const updatehotline = await models.mongo.hotlines.updateMany(
        { Code: { $in: hotline_codes } },
        hotlineCs
      );

      if (updatehotline) {
        io.emit(jwtConf.socketType.REQEST_RELOAD_HOTLINE, { status: true });
        let hotlineUpdates = await models.mongo.hotlines.find({
          Code: { $in: hotline_codes },
        });
        let dataLog = hotlineUpdates.map((value) => {
          let data = {
            BrandLogs: jwtConf.BrandLogsType.HOTLINE,
            Code: randomId(12, "A0"),
            Type: jwtConf.logType.UPDATE,
            Hotline_code: value.Code,
            Tenant_code: user.Code,
            Hotline_info: value,
            Update_key: [jwtConf.logTypeKey.STATUS_WAITINGBRANDNAME],
            Tenant_role: user.Role,
          };
          return data;
        });
        await logs.create(dataLog);
        return Promise.resolve(updatehotline);
      } else
        return Promise.reject({
          show: true,
          message: "Xảy ra lỗi, xin vui lòng thử lại",
        });
    default:
      break;
  }
};

exports.list = async (user, body, options = {}) => {
  let {
    limit,
    offset,
    sort_by,
    hotline,
    Type_NetWorks,
    from_date,
    to_date,
    BrandName,
    CustomerCodes,
    Statuslist,
  } = options;
  let schema = Joi.object().keys({
    offset: Joi.number().integer(),
    limit: Joi.number().integer(),
    sort_by: Joi.string(),
    hotline: Joi.string(),
    netWorkId_list: Joi.array(),
  });
  let result = schema.validate(options, { allowUnknown: true });
  if (result.error) {
    return Promise.reject({ show: true, message: result.error.message });
  }
  if (user.Role == jwtConf.roles.CUSTOMER) {
    Statuslist = [jwtConf.hotlineStatus.READY];
  }

  let condition = {};
  if (hotline) {
    condition["Hotline"] = { $regex: options.hotline.toLowerCase() };
  }
  if (BrandName) {
    condition["BrandName"] = { $regex: options.BrandName.toLowerCase() };
  }

  if (Type_NetWorks?.length > 0) {
    condition["Type_NetWork"] = { $in: Type_NetWorks };
  }

  if (CustomerCodes?.length > 0) {
    condition["CustomerCode"] = { $in: CustomerCodes };
  }

  if (Statuslist?.length > 0) {
    condition["Status"] = { $in: Statuslist };

    // conditions.push({ Status: { $in: Statuslist } });
  }

  // Kết hợp các điều kiện bằng toán tử AND
  let query = models.mongo.hotlines.find(condition);
  let count = models.mongo.hotlines.countDocuments(condition);

  if (from_date) {
    // from = moment(from_date, "YYYY-MM-DD HH:mm").toDate();
    const fromDate = new Date(from_date);
    query.where("Created_at").gt(fromDate);
    count.where("Created_at").gt(fromDate);
  }

  if (to_date) {
    // to = moment(to_date, "YYYY-MM-DD HH:mm").toDate();
    const toDate = new Date(to_date);
    query.where("Created_at").lt(toDate);
    count.where("Created_at").lt(toDate);
  }

  if (sort_by) {
    let sort = sort_by.split(":");
    let sort_obj = {};
    sort_obj[sort[0]] = sort[1];
    query.sort(sort_obj);
  } else {
    query.sort({ Created_at: -1 });
  }

  if (limit) {
    query.limit(parseInt(limit));
  } else {
    query.limit(10);
  }
  if (offset) {
    query.skip(parseInt(offset));
  } else {
    query.skip(0);
  }

  let datas = await Promisebb.props({
    data: query,
    total: count,
  });

  let newdata = [];
  for (let i = 0; i < datas.data.length; i++) {
    let value = JSON.parse(JSON.stringify(datas.data[i]));

    if (datas.data[i].BrandName) {
      const checkBrandname = await models.mongo.brandnames.findOne({
        Code: datas.data[i].BrandName,
      });
      if (checkBrandname) {
        value.BrandnameInfo = checkBrandname;
      }
    }

    if (datas.data[i].CustomerCode) {
      const checkCustomer = await models.mongo.customers.findOne({
        enterprise_number: datas.data[i].CustomerCode,
      });

      if (checkCustomer) {
        value.CustomerInfo = checkCustomer;
      } else {
        const checkCustomers = await models.mongo.accounts.findOne({
          enterprise_number: datas.data[i].CustomerCode,
        });
        if (checkCustomers) {
          value.CustomerInfo = checkCustomers;
        }
      }
    }
    newdata.push(value);
  }

  return Promise.resolve({
    list: newdata,
    total: datas.total,
  });
};
