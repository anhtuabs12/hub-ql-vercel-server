let models = require("../models");
const jwtConf = require("../../config/jwt");
const randomId = require("random-id");
const logs = require("./logs");
const Joi = require("joi");
const Promisebb = require("bluebird");

exports.create = async (user, body = {}) => {
  let schema = Joi.object().keys({
    Type: Joi.string().required(),
    Value: Joi.string().required(),
    Title: Joi.string().required(),
  });

  if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
    return Promise.reject({
      show: true,
      message: "Không có quền thêm tạo rule",
    });
  } else {
    let errs = schema.validate(body, { allowUnknown: true });
    let err = errs.err;
    if (err) {
      return Promise.reject({ show: true, message: err.message });
    }
    body.Value = body.Value.trim();

    let rules = {
      Code: randomId(12, "A0"),
      Type: body.Type,
      Value: body.Value,
      Title: body.Title,
      Created_at: new Date(),
      Updated_at: new Date(),
    };

    const ruleres = await models.mongo.rules(rules).save();
    if (ruleres) {
      return Promise.resolve(ruleres);
    } else {
      return Promise.reject({
        show: true,
        message: "Xảy ra lỗi, xin vui lòng thử lại",
      });
    }
  }
};

exports.update = async (rule_code, user, body = {}) => {
  if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
    return Promise.reject({
      show: true,
      message: "Không có quền sửa rule",
    });
  } else {
    const checkRule_code = await models.mongo.rules.findOne({
      Code: rule_code,
    });

    if (!checkRule_code) {
      return Promise.reject({
        show: true,
        message: "Rule không tồn tại",
      });
    }

    let rules = {
      ...body,
      Updated_at: new Date(),
    };

    const ruleres = await models.mongo.rules.updateOne(
      { Code: rule_code },
      rules,
      { new: true }
    );
    if (ruleres) {
      return Promise.resolve(ruleres);
    } else
      return Promise.reject({
        show: true,
        message: "Xảy ra lỗi, xin vui lòng thử lại",
      });
  }
};

exports.list = async (user,body = {},option ={}) => {
  if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
    return Promise.reject({
      show: true,
      message: "Không có quền xem rule",
    });
  } else {
    let query = await models.mongo.rules.find();
    let count = await models.mongo.rules.countDocuments();
    return Promise.resolve({
        list: query,
        total: count,
    });
  }
};

