let models = require("../models");
const randomId = require("random-id");
const bcrypt = require("bcryptjs");
const Joi = require("joi");
const Promisebb = require("bluebird");
const _ = require("lodash");
const jwtConf = require("../../config/jwt");
var jwt = require("jsonwebtoken");
const logs = require("./logs");

exports.create = async (user, body = {}) => {
  let schema = Joi.object().keys({
    FullName: Joi.string().required(),
    Email: Joi.string().required(),
    PhoneNumber: Joi.string().required(),
    Role: Joi.string().required(),
  });

  let schema2 = Joi.object().keys({
    enterprise_number: Joi.string().required(),
    companyname: Joi.string().required(),
    Role: Joi.string().required(),
  });

  const checktanent = await models.mongo.accounts.findOne({ Code: user.Code });
  if (checktanent && checktanent.Deleted) {
    return Promise.reject({
      show: true,
      message: "Accout của bạn đã bị thu hồi",
    });
  }

  if (body.Role == jwtConf.roles.CUSTOMER) {
    if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
      return Promise.reject({
        show: true,
        message: "Không có quền thêm tạo tài khoản",
      });
    }
    let { error, value } = schema2.validate(body, { allowUnknown: true });
    if (error) {
      return Promise.reject({
        show: true,
        message: error.details[0].message,
      });
    }

    const checkAccountCustomer = await models.mongo.accounts.findOne({
      Role: body.Role,
      enterprise_number: body.enterprise_number,
    });

    if (checkAccountCustomer) {
      return Promise.reject({
        show: true,
        message: `Số đại diện bị trùng với  ${checkAccountCustomer.companyname}`,
      });
    }

    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(`${jwtConf.password_default}`, salt);

    let account = {
      Code: randomId(12, "A0"),
      Password: hash,
      companyname: body.companyname,
      enterprise_number: body.enterprise_number,
      Deleted: false,
      Role: body.Role,
      Tenant_code: user.Code,
      Created_at: new Date(),
      Updated_at: new Date(),
    };
    let accounts = await models.mongo.accounts(account).save();
    if (accounts) {
      return Promise.resolve(accounts);
    } else
      return Promise.reject({
        show: true,
        message: "Xảy ra lỗi, xin vui lòng thử lại",
      });
  } else {
    if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
      return Promise.reject({
        show: true,
        message: "Không có quền thêm tạo tài khoản",
      });
    } else {
      let { error, value } = schema.validate(body, { allowUnknown: true });
      if (error)
        return Promise.reject({
          show: true,
          message: error.details[0].message,
        });

      body.FullName = body.FullName ? body.FullName.toLowerCase().trim() : "";
      body.Email = body.Email ? body.Email.toLowerCase().trim() : "";
      body.PhoneNumber = body.PhoneNumber
        ? body.PhoneNumber.toLowerCase().trim()
        : "";

      if (body.Email) {
        let checkExists = await models.mongo.accounts.findOne({
          Email: body.Email,
        });
        if (checkExists) {
          return Promise.reject({
            show: true,
            message: "Account đã tồn tại",
          });
        }
      }

      var salt = bcrypt.genSaltSync(10);
      var hash = bcrypt.hashSync(`${jwtConf.password_default}`, salt);

      let account = {
        Code: randomId(12, "A0"),
        Password: hash,
        FullName: body.FullName,
        Email: body.Email,
        Deleted: false,
        Role: body.Role,
        Tenant_code: user.Code,
        Created_at: new Date(),
        Updated_at: new Date(),
      };

      let accounts = await models.mongo.accounts(account).save();
      if (accounts) {
        return Promise.resolve(accounts);
      } else
        return Promise.reject({
          show: true,
          message: "Xảy ra lỗi, xin vui lòng thử lại",
        });
    }
  }
};

exports.loginStaff = async (body = {}, io) => {
  let schema = Joi.object().keys({
    Email: Joi.string().required(),
    Password: Joi.string().required(),
  });

  let errs = schema.validate(body, { allowUnknown: true });
  let err = errs.err;
  if (err) return Promise.reject({ show: true, message: err.message });
  let ttl = body.ttl_in_second || 24 * 60 * 60 * 30;

  let account = await models.mongo.accounts.findOne({
    Email: body.Email,
  });

  if (!account) {
    return Promise.reject({ show: true, message: "Tài khoản không tồn tại" });
  }

  if (account.deleted) {
    return Promise.reject({ show: true, message: "Tài khoản không tồn tại" });
  }

  return bcrypt.compare(body.Password, account.Password).then(function (match) {
    if (!match)
      return Promise.reject({
        show: true,
        message: "Mật khẩu không chính xác",
      });
    let token = jwt.sign(
      {
        Email: account.Email,
        FullName: account.FullName,
        Tenant_code: account.Tenant_code,
        Code: account.Code,
        Deleted: account.Deleted,
        Role: account.Role,
        typeLogin: 1,
        Created_at: account.Created_at,
        Updated_at: account.Updated_at,
      },
      jwtConf.secret_token,
      {
        expiresIn: ttl,
      }
    );

    if (account.Deleted) {
      return Promise.reject({
        show: true,
        message: "Tài khoản không tồn tại",
      });
    }

    if (token) {
      io.emit(jwtConf.socketType.CHECK_LOGINED, {
        newToken: token,
        key: account.Code,
      });
      return Promise.resolve({
        token: token,
        account_code: account.Code,
        ttl: ttl,
        info: account,
      });
    } else return Promise.reject({ show: true, message: "Xảy ra lỗi, xin vui lòng thử lại" });
  });
};

exports.loginCustomer = async (body = {}, io) => {
  let schema = Joi.object().keys({
    enterprise_number: Joi.string().required(),
    password: Joi.string().required(),
  });

  let errs = schema.validate(body, { allowUnknown: true });
  let err = errs.err;
  if (err) return Promise.reject({ show: true, message: err.message });
  let ttl = body.ttl_in_second || 24 * 60 * 60 * 30;

  let customer = await models.mongo.customers.findOne({
    enterprise_number: body.enterprise_number,
  });

  if (!customer) {
    return Promise.reject({ show: true, message: "Tài khoản không tồn tại" });
  }

  if (body.password === jwtConf.password_default) {
    let token = jwt.sign(
      {
        enterprise_number: customer.enterprise_number,
        compayname: customer.compayname,
        typeLogin: 2,
        Role: jwtConf.roles.CUSTOMER,
        Logind_at: new Date(),
      },
      jwtConf.secret_token,
      {
        expiresIn: ttl,
      }
    );

    if (token) {
      io.emit(jwtConf.socketType.CHECK_LOGINED, {
        newToken: token,
        key: customer.enterprise_number,
      });
      return Promise.resolve({
        token: token,
        ttl: ttl,
        info: customer,
      });
    } else
      return Promise.reject({
        show: true,
        message: "Xảy ra lỗi, xin vui lòng thử lại",
      });
  } else {
    return Promise.reject({ show: true, message: "mật khẩu không chính xác" });
  }
};

exports.delete = async (account_code, user, body, io) => {
  if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
    return Promise.reject({
      show: true,
      message: "Không có quền xóa tạo tài khoản",
    });
  }

  const checkAccount_code = await models.mongo.accounts.findOne({
    Code: account_code,
  });
  if (checkAccount_code.Deleted) {
    return Promise.reject({
      show: true,
      message: "Tài khoản không tồn tại",
    });
  }
  if (!checkAccount_code) {
    return Promise.reject({
      show: true,
      message: "Tài khoản không tồn tại",
    });
  } else {
    let updated = { Deleted: true, Updated_at: new Date() };
    const deleteaction = await models.mongo.accounts.updateOne(
      {
        Code: account_code,
      },
      updated
    );

    if (deleteaction) {
      io.emit(jwtConf.socketType.CHECK_LOGINED, "logout");
      return Promise.resolve(deleteaction);
    } else {
      return Promise.reject({
        show: true,
        message: "Xảy ra lỗi, xin vui lòng thử lại",
      });
    }
  }
};

exports.list = async (user, body, options = {}) => {
  let { limit, offset, sort_by, keywords, Role } = options;
  let schema = Joi.object().keys({
    offset: Joi.number().integer(),
    limit: Joi.number().integer(),
    sort_by: Joi.string(),
    keywords: Joi.string(),
  });
  let result = schema.validate(options, { allowUnknown: true });
  if (result.error) {
    return Promise.reject({ show: true, message: result.error.message });
  }

  const account = await models.mongo.accounts.findOne({ Code: user.Code });

  if (account.Deleted) {
    return Promise.reject({
      show: true,
      message: "Tài khoản không tồn tại",
    });
  }

  let condition = {};
  if (keywords) {
    condition["$or"] = [
      { FulName: { $regex: keywords, $options: "i" } },
      { Email: { $regex: keywords, $options: "i" } },
    ];
  }

  if (Role) {
    condition["Role"] = { $in: Role };
  }

  condition["Deleted"] = false;
  let query = models.mongo.accounts.find(condition);
  let count = models.mongo.accounts.countDocuments(condition);

  if (sort_by) {
    let sort = sort_by.split(":");
    let sort_obj = {};
    sort_obj[sort[0]] = sort[1];
    query.sort(sort_obj);
  } else {
    query.sort({ code: -1 });
  }

  if (limit) {
    query.limit(parseInt(limit));
  } else {
    query.limit(20);
  }
  if (offset) {
    query.skip(parseInt(offset));
  } else {
    query.skip(0);
  }

  let datas = await Promisebb.props({
    data: query,
    total: count,
  });

  return Promise.resolve({
    list: datas.data,
    total: datas.total,
  });
};

exports.logined = async (user, options = {}) => {
  if (user.typeLogin == 1) {
    const accountInfo = await models.mongo.accounts.findOne({
      Email: user.Email,
    });
    if (accountInfo.Deleted) {
      return Promise.reject({
        show: true,
        message: "Tài khoản không tồn tại",
      });
    }
    return Promise.resolve({ user, info: accountInfo });
  } else {
    const accountInfo = await models.mongo.customers.findOne({
      enterprise_number: user.enterprise_number,
    });
    return Promise.resolve({
      user,
      info: { ...user, Role: jwtConf.roles.CUSTOMER },
    });
  }
};

exports.createdev = async (body = {}) => {
  let schema = Joi.object().keys({
    FullName: Joi.string().required(),
    Email: Joi.string().required(),
    PhoneNumber: Joi.string().required(),
    Role: Joi.string().required(),
  });
  let errs = schema.validate(body, { allowUnknown: true });
  let err = errs.err;
  if (err) return Promise.reject({ show: true, message: err.message });

  body.FullName = body.FullName ? body.FullName.toLowerCase().trim() : "";
  body.Email = body.Email ? body.Email.toLowerCase().trim() : "";
  body.PhoneNumber = body.PhoneNumber
    ? body.PhoneNumber.toLowerCase().trim()
    : "";

  if (body.Email) {
    let checkExists = await models.mongo.accounts.findOne({
      Email: body.Email,
    });
    if (checkExists) {
      return Promise.reject({
        show: true,
        message: "Account đã tồn tại",
      });
    }
  }

  var salt = bcrypt.genSaltSync(10);
  var hash = bcrypt.hashSync(`${jwtConf.password_default}`, salt);

  let account = {
    Code: randomId(10, "A0"),
    Password: hash,
    FullName: body.FullName,
    Email: body.Email,
    Deleted: false,
    Role: body.Role,
    Tenant_code: "dev",
    Created_at: new Date(),
    Updated_at: new Date(),
  };


  let accounts = await models.mongo.accounts(account).save();
  if (accounts) {
    return Promise.resolve(accounts);
  } else
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, xin vui lòng thử lại",
    });
};

exports.findbycode = async (AccountCode, user) => {
  if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
    return Promise.reject({
      show: true,
      message: "Không có quyền truy cập",
    });
  }

  const accountInfo = await models.mongo.accounts.findOne({
    Code: AccountCode,
  });
  if (accountInfo) {
    return Promise.resolve({ user, info: accountInfo });
  } else {
    return Promise.reject({ show: true, message: "lỗi" });
  }
  s;
};

exports.update = async (AccountCode, user, body, io) => {
  if (user.Role !== jwtConf.roles.ADMINISTRATOR) {
    return Promise.reject({
      show: true,
      message: "Không có quyền truy cập",
    });
  }

  let newAccount = {
    ...body,
    Updated_at: new Date(),
  };

  const account = await models.mongo.accounts.updateOne(
    {
      Code: AccountCode,
    },
    newAccount
  );
  if (account) {
    io.emit(jwtConf.socketType.CHECK_LOGINED, "logout");
    return Promise.resolve(account);
  } else {
    return Promise.reject({ show: true, message: "lỗi" });
  }
};
