let models = require("../models");
const jwtConf = require("../../config/jwt");
const randomId = require("random-id");
const logs = require("./logs");
const Joi = require("joi");
const Promisebb = require("bluebird");
const hotlineLib = require("./hotline");

exports.create = async (user, body = {}) => {
  if (user.Role == jwtConf.roles.CUSTOMER) {
    return Promise.reject({
      show: true,
      message: "Không có quyền",
    });
  }
  let schema = Joi.object().keys({
    Type: Joi.string().required(),
    Hotlines_codes: Joi.array().required(),
    Status: Joi.string().required(),
    Tenant_code: Joi.string().required(),
    ApproveBy_code: Joi.string(),
    Ticket_from: Joi.string().required(),
    Description: Joi.string(),
  });

  let errs = schema.validate(body, { allowUnknown: true });
  let err = errs.err;
  if (err) {
    return Promise.reject({ show: true, message: err.message });
  }

  let ticket = {
    // Type: body.Type,
    Tenant_code: user.Code,
    Hotlines_codes: { $in: body.Hotlines_codes },
    Status: jwtConf.statusType.WAITING,
  };
  const ticketCheck = await models.mongo.tickets.findOne(ticket).exec();
  if (ticketCheck) {
    return Promise.reject({
      show: true,
      message: "Hotline đang nằm trong ticket gần nhất của bạn!",
    });
  }

  body.Description = body.Description.trim();

  let tickets = {
    Code: randomId(12, "A0"),
    Type: body.Type,
    Hotlines_codes: body.Hotlines_codes,
    Status: body.Status,
    Tenant_code: user.Code,
    ApproveBy_code: null,
    Ticket_from: body.Ticket_from,
    Description: body.Description,
    Created_at: new Date(),
    Updated_at: new Date(),
  };

  const ticketres = await models.mongo.tickets(tickets).save();
  if (ticketres) {
    return Promise.resolve(ticketres);
  } else {
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, xin vui lòng thử lại",
    });
  }
};

exports.update = async (ticket_code, user, body = {}, io) => {
  if (user.Role == jwtConf.roles.CUSTOMER) {
    return Promise.reject({
      show: true,
      message: "Không có quyền",
    });
  }
  let tickets = {
    ApproveBy_code: user.Code,
  };
  const checkTicket_code = await models.mongo.tickets.findOne({
    Code: ticket_code,
  });

  if (!checkTicket_code) {
    return Promise.reject({
      show: true,
      message: "Ticket không tồn tại",
    });
  }

  if (user.Role == jwtConf.roles.ADMINISTRATOR) {
    if (
      checkTicket_code.Status == jwtConf.statusType.AGREE ||
      checkTicket_code.Status == jwtConf.statusType.DISAGREE
    ) {
      return Promise.reject({
        show: true,
        message: "Không hợp lệ",
      });
    }

    tickets = {
      Status: body.Status,
      Updated_at: new Date(),
    };
  } else {
    if (
      checkTicket_code.Status == jwtConf.statusType.WAITING &&
      body.Status == jwtConf.statusType.DISAGREE
    ) {
      tickets = {
        Status: body.Status,
        Updated_at: new Date(),
      };
    } else {
      return Promise.reject({
        show: true,
        message: "Không hợp lệ",
      });
    }
  }
  let hotlinelist = await models.mongo.hotlines.find({
    Hotline: { $in: checkTicket_code.Hotlines_codes },
  });
  let hotlineCodeList = hotlinelist.map((value) => value.Code);
  if (body.Status == jwtConf.statusType.AGREE) {
    switch (checkTicket_code.Type) {
      case jwtConf.ticketType.STATUS_FROM_USE:
        let payloadSTATUS_FROM_USE = {
          hotline_codes: hotlineCodeList,
        };
        await hotlineLib.evicting(user, payloadSTATUS_FROM_USE, io);
        break;
      case jwtConf.ticketType.CANCEL_NUMBER:
        let payloadCANCEL_NUMBER = {
          hotline_codes: hotlineCodeList,
        };
        await hotlineLib.canceling(user, payloadCANCEL_NUMBER, io);
        break;
      case jwtConf.ticketType.UNUSED:
        let payloadUNUSED = {
          hotline_codes: hotlineCodeList,
        };
        await hotlineLib.ready(user, payloadUNUSED, io);
        break;
      default:
        break;
    }
  }

  const ticketres = await models.mongo.tickets.updateOne(
    { Code: ticket_code },
    tickets,
    { new: true }
  );
  if (body.Status == jwtConf.statusType.AGREE) {
    switch (checkTicket_code.Type) {
      case jwtConf.ticketType.WITHDRAWAL:
        let payload = {
          hotline_codes: [...checkTicket_code.Hotlines_codes],
        };
        await hotlineLib.evicting(user, payload);
        break;

      default:
        break;
    }
  }
  if (ticketres) {
    return Promise.resolve(ticketres);
  } else
    return Promise.reject({
      show: true,
      message: "Xảy ra lỗi, xin vui lòng thử lại",
    });
};

exports.list = async (user, body = {}, options = {}) => {
  let {
    Ticket_ApproveBy_code,
    Ticket_Tenant_code,
    Ticket_Code,
    Hotline,
    Ticket_Status,
    Ticket_Type,
    limit,
    offset,
    sort_by,
    from_date,
    to_date,
    Tenant_codes,
  } = options;
  let schema = Joi.object().keys({
    offset: Joi.number().integer(),
    limit: Joi.number().integer(),
    from: Joi.string(),
    to: Joi.string(),
    Tenant_codes: Joi.array(),
    ApproveBy_codes: Joi.array(),
    Ticket_Type: Joi.array(),
    Ticket_Status: Joi.array(),
    Hotline: Joi.string(),
    Ticket_Code: Joi.string(),
    Ticket_Tenant_code: Joi.array(),
    Ticket_ApproveBy_code: Joi.array(),
  });
  let result = schema.validate(options, { allowUnknown: true });
  if (result.error) {
    return Promise.reject({ show: true, message: result.error.message });
  }
  let condition = {};

  if (user.Role == jwtConf.roles.CUSTOMER) {
    return Promise.reject({
      show: true,
      message: "Không có quền xem",
    });
  }
  if (user.Role == jwtConf.roles.SALEADMIN) {
    condition["Tenant_code"] = user.Code;
  }

  if (user.Role == jwtConf.roles.ADMINISTRATOR) {
    if (Tenant_codes) {
      condition["Tenant_code"] = { $in: Tenant_codes };
    }
  }
  if (Ticket_Type?.length > 0) {
    condition["Type"] = { $in: Ticket_Type };
  }

  if (Ticket_Status?.length > 0) {
    condition["Status"] = { $in: Ticket_Status };
  }
  if (Hotline) {
    condition["Hotlines_codes"] = { $regex: options.Hotline.toLowerCase() };
  }
  if (Ticket_Code) {
    condition["Code"] = { $regex: options.Ticket_Code.toUpperCase() };
  }

  if (Ticket_Tenant_code?.length > 0) {
    condition["Tenant_code"] = { $in: Ticket_Tenant_code };
  }
  if (Ticket_ApproveBy_code?.length > 0) {
    condition["ApproveBy_code"] = { $in: Ticket_ApproveBy_code };
  }
  let query = models.mongo.tickets.find(condition);
  let count = models.mongo.tickets.countDocuments(condition);

  if (from_date) {
    // from = moment(from_date, "YYYY-MM-DD HH:mm").toDate();
    const fromDate = new Date(from_date);
    query.where("Created_at").gt(fromDate);
    count.where("Created_at").gt(fromDate);
  }

  if (to_date) {
    // to = moment(to_date, "YYYY-MM-DD HH:mm").toDate();
    const toDate = new Date(to_date);
    query.where("Created_at").lt(toDate);
    count.where("Created_at").lt(toDate);
  }

  if (sort_by) {
    let sort = sort_by.split(":");
    let sort_obj = {};
    sort_obj[sort[0]] = sort[1];
    query.sort(sort_obj);
  } else {
    query.sort({ Created_at: -1 });
  }

  if (limit) {
    query.limit(parseInt(limit));
  } else {
    query.limit(10);
  }
  if (offset) {
    query.skip(parseInt(offset));
  } else {
    query.skip(0);
  }

  let datas = await Promisebb.props({
    data: query,
    total: count,
  });

  return Promise.resolve({
    list: datas.data,
    total: datas.total,
  });
};
