let models = require("../models");
const jwtConf = require("../../config/jwt");
const randomId = require("random-id");
const logs = require("./logs");
const Joi = require("joi");
const Promisebb = require("bluebird");

exports.list = async (user, body = {}, option = {}) => {
  let query = await models.mongo.customers.find();
  let count = await models.mongo.customers.countDocuments();
  return Promise.resolve({
    list: query,
    total: count,
  });
};
