"use strict";

const account = require("./account");
const common = require("./common");
const hotline = require("./hotline");
const rule = require("./rule");
const ticket = require("./ticket");
const brandname = require("./brandname");
const log = require("./log");

const customer = require("./customer");

module.exports = function (app, io) {
  hotline(app, io);
  account(app, io);
  common(app, io);
  rule(app, io);
  ticket(app,io);
  brandname(app,io);
  log(app,io)
  customer(app,io);

  app._router.stack.forEach(function (r) {
    if (r.route && r.route.path) {
      var keys = Object.keys(r.route.methods);
      keys.forEach(function (key) {
        console.log(key + " - " + r.route.path);
      });
    }
  });
};
