"use strict";

var resp = require("../response");
// var common = require('../lib/common');
var multer = require("multer");
var fs = require("fs");
var moment = require("moment");
var mime = require("mime");
const randomId = require("random-id");

var DIR = "./uploads/";
var upload = multer({ dest: DIR });
const jwtConf = require("../../config/jwt");
var middlewarePermission = require("../middleware/checkPermission");
const xlsx = require("node-xlsx").default;
const axios = require("axios");
var FormData = require("form-data");
const models = require("../models");
const exceljs = require("exceljs");

const uploads = multer({ storage: multer.memoryStorage() });

module.exports = function (app, passport) {
  app
    .route("/upload")
    .post([middlewarePermission], upload.single("file"), (req, res) => {
      let file_info = JSON.parse(JSON.stringify(req.file));
      let file_name = file_info.originalname;
      if (
        (file_name && file_name.includes("%")) ||
        file_name.includes("/") ||
        file_name.includes("*") ||
        file_name.includes("?") ||
        file_name.includes("\\")
      ) {
        return resp.throws(res, req, { message: "Tên file không hợp lệ" });
      }
      if (
        /.*\.(docx|doc|xlsx|xls|pdf|png|jpg|jpeg)$/.test(
          file_name.toLowerCase()
        ) === false
      ) {
        return resp.throws(res, req, {
          message:
            "Định dạng file không đúng (docx, doc, xlsx, xls, pdf,png, jpg, jpeg)",
        });
      }
      if (file_info.size > 5242880) {
        return resp.throws(res, req, {
          message: "Kích thước file vượt quá 5MB",
        });
      }
      var file = "uploads/" + req.file.originalname;
      var file_arr = req.file.originalname.split(".");
      file =
        "uploads/" + moment().valueOf() + "." + file_arr[file_arr.length - 1];
      fs.rename(req.file.path, file, function (err) {
        if (err) {
          return resp.throws(res, req, err);
        } else {
          if (req.query.type == "json") {
            let data = {
              link: process.env.fullcontextpath + file,
              name: file,
            };
            return resp.sendOK(res, req, data);
          } else {
            return resp.sendOK(res, req, file);
          }
        }
      });
    });

  app
    .route("/importhotline")
    .post([middlewarePermission], uploads.single("file"), async (req, res) => {
      console.log(req.file, "files");
      if (!req.file || Object.keys(req.file).length === 0) {
        return res.status(400).send("Không có file nào được tải lên.");
      }

      const file = req.file;

      let data = await handleFileUpload(file, req.user)
      // console.log(data);
      res.status(200).send(data);
    });
};

async function handleFileUpload(file, user) {
  const workbook = new exceljs.Workbook();
  await workbook.xlsx.load(file.buffer);

  const worksheet = workbook.getWorksheet(1);
  let datalist = []

  worksheet.eachRow(async (row, rowNumber) => {
    if (rowNumber !== 1) {
      // Bỏ qua hàng tiêu đề
      const hotlineData = {
        Hotline: row.getCell(2).value,
        Type_NetWork: row.getCell(3).value,
        Status: row.getCell(4).value,
        CustomerName: row.getCell(5).value,
        Lunch_date: row.getCell(6).value,
        BrandName: row.getCell(7).value,
        Tanent_code: user.Code,
        PurchaseDate: row.getCell(8).value,
      };

      hotlineData.PurchaseDate = new Date(
        hotlineData.PurchaseDate || Date.now()
      );
      if (hotlineData.Lunch_date) {
        hotlineData.Lunch_date = new Date(hotlineData.Lunch_date);
      }

      if (hotlineData.Hotline) {
        hotlineData.Hotline = "0" + hotlineData.Hotline;
      }
      if (hotlineData.Type_NetWork) {
        switch (hotlineData.Type_NetWork) {
          case "Viettel":
            hotlineData.Type_NetWork = jwtConf.netWorkId.VIETTEL;
            break;
          case "Vina":
            hotlineData.Type_NetWork = jwtConf.netWorkId.VINA;
            break;
          case "Mobi":
            hotlineData.Type_NetWork = jwtConf.netWorkId.MOBI;
            break;

          default:
            break;
        }
      }

      if(hotlineData.CustomerName){
        const customer = await models.mongo.customers.find({ companyname: { $regex: new RegExp(hotlineData.CustomerName, "i") } })
        if(customer.length !== 1){
          return Promise.reject({
            show: true,
            message: "Tên khách hàng bị trùng lặp với 2 khách hàng ",
          });
        }
        hotlineData.CustomerCode = customer[0].enterprise_number
      }
      let hotlinesPayload = {
        code: randomId(12, "A0"),
        Type_NetWork: hotlineData.Type_NetWork, //id nhà mạng
        Hotline: hotlineData.Hotline, // hotline
        PurchaseDate: hotlineData.PurchaseDate, //ngày mua
        CustomerCode: hotlineData.CustomerCode || null, // code khách hàng
        Lunch_date: hotlineData.Lunch_date || null, // ngày cấp hotline cho khách hàng
        Status: hotlineData.Status || null,
        BrandName: hotlineData.BrandName || null,
        Created_at: new Date(),
        Updated_at: new Date(),
        Description: hotlineData.Description,
        Tanent_code: user.Code
      };
      datalist = [...datalist, hotlinesPayload]
      console.log(datalist.length);
  console.log(datalist);


    }
  });
  console.log("datalist"  ,datalist);

  return datalist
}
