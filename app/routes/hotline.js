"use strict";

var resp = require("../response");
var hotline = require("../lib/hotline");
var middlewarePermission = require("../middleware/checkPermission");

module.exports = function (app, io) {
  app.route("/hotline").post([middlewarePermission], (req, res) => {
    hotline
      .create(req.user,  req.body, io)
      .then((ok) => {
        resp.sendOK(res, req, ok);
      })
      .catch(function (err) {
        resp.throws(res, req, err);
      });
  }),
    app
      .route("/hotline/:hotline_code")
      .put([middlewarePermission], (req, res) => {
        hotline
          .update(req.params.hotline_code, req.user, req.body, io)
          .then((ok) => {
            resp.sendOK(res, req, ok);
          })
          .catch(function (err) {
            resp.throws(res, req, err);
          });
      });
  app
    .route("/hotline/:hotline_code")
    .delete([middlewarePermission], (req, res) => {
      hotline
        .delete(req.params.hotline_code , req.user)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch(function (err) {
          resp.throws(res, req, err);
        });
    });

    app
    .route("/listhotline")
    .post([middlewarePermission], (req, res) => {
      hotline
        .list(req.user, req.body , req.body)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch(function (err) {
          resp.throws(res, req, err);
        });
    });

    app
    .route("/keepinghotline")
    .post([middlewarePermission], (req, res) => {
      hotline
        .keeping(req.user, req.body , io)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch(function (err) {
          resp.throws(res, req, err);
        });
    });

    app
    .route("/evictinghotline")
    .post([middlewarePermission], (req, res) => {
      hotline
        .evicting(req.user, req.body , io)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch(function (err) {
          resp.throws(res, req, err);
        });
    });

    app
    .route("/readyhotline")
    .post([middlewarePermission], (req, res) => {
      hotline
        .ready(req.user, req.body ,io)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch(function (err) {
          resp.throws(res, req, err);
        });
    });

    app
    .route("/cancelinghotline")
    .post([middlewarePermission], (req, res) => {
      hotline
        .canceling(req.user, req.body , io)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch(function (err) {
          resp.throws(res, req, err);
        });
    });

    app
    .route("/doinghotline")
    .post([middlewarePermission], (req, res) => {
      hotline
        .doing(req.user, req.body , io)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch(function (err) {
          resp.throws(res, req, err);
        });
    });

    
    app
    .route("/waitingbrandnamehotline")
    .post([middlewarePermission], (req, res) => {
      hotline
        .waitingbrandname(req.user, req.body , io)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch(function (err) {
          resp.throws(res, req, err);
        });
    });

    
    app
    .route("/hotline/:hotline_code")
    .get([middlewarePermission], (req, res) => {
      hotline
        .findbycode(req.params.hotline_code,req.user)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch(function (err) {
          resp.throws(res, req, err);
        });
    });
};
