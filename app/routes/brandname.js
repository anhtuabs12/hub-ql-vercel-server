"use strict";

var resp = require("../response");
var brandname = require("../lib/brandname");
var middlewarePermission = require("../middleware/checkPermission");

module.exports = function (app, passport) {
  app.route("/brandname").post([middlewarePermission], (req, res) => {
    brandname
      .create(req.user, req.body)
      .then((ok) => {
        resp.sendOK(res, req, ok);
      })
      .catch((err) => {
        resp.throws(res, req, err);
      });
  });
  app
    .route("/brandname/:brandname_code")
    .put([middlewarePermission], (req, res) => {
      brandname
        .update(req.params.brandname_code, req.user, req.body)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch((err) => {
          resp.throws(res, req, err);
        });
    });
  app
    .route("/brandname/:brandname_code")
    .delete([middlewarePermission], (req, res) => {
      brandname
        .delete(req.params.brandname_code, req.user)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch((err) => {
          resp.throws(res, req, err);
        });
    });
  app.route("/listbrandname").post([middlewarePermission], (req, res) => {
    brandname
      .list(req.user, req.body)
      .then((ok) => {
        resp.sendOK(res, req, ok);
      })
      .catch((err) => {
        resp.throws(res, req, err);
      });
  });
  app
    .route("/brandname/:brandname_code")
    .get([middlewarePermission], (req, res) => {
        brandname
        .findbycode(req.params.brandname_code, req.user)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch(function (err) {
          resp.throws(res, req, err);
        });
    });
};
