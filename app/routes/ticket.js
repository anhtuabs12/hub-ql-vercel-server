"use strict";

var resp = require("../response");
var ticket = require("../lib/ticket");
var middlewarePermission = require("../middleware/checkPermission");
const io = require("../../server");

module.exports = function (app, io) {
    app.route("/ticket").post([middlewarePermission],(req, res) => {
        ticket
        .create(req.user, req.body)
        .then((ok)=>{
            resp.sendOK(res, req, ok);
        })
        .catch(function(err){
            resp.throws(res, req, err);
        })
    });

    app.route("/ticket/:ticket_code").put([middlewarePermission],(req,res)=>{
        ticket
        .update(req.params.ticket_code, req.user, req.body,io)
        .then((ok)=>{
            resp.sendOK(res, req, ok);
        })
        .catch(function(err){
            resp.throws(res,req,err);
        })
    });

    app.route("/ticket/:ticket_code").delete([middlewarePermission], (req, res)=>{
        ticket
        .delete(req.params.ticket_code, req.user, req.body)
        .then((ok)=>{
            resp.sendOK(res,req,ok);
        })
        .catch(function(err){
            resp.throws(res, req, err);
        })
    });

    app.route("/listticket").post([middlewarePermission],(req, res)=>{
        ticket
        .list(req.user, req.body, req.body)
        .then((ok)=>{
            resp.sendOK(res,req,ok);
        })
        .catch(function(err){
            resp.throws(res,req,err);
        })
    });

    app.route("/ticketbycode/:ticket_code").get([middlewarePermission],(req,res)=>{
        ticket
        .findbycode(req.params.rule_code)
        .then((ok)=>{
            resp.sendOK(res,req,ok);
        })
        .catch((err)=>{
            resp.throws(res,req,err);
        })
    });
};