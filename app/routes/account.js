"use strict";

var resp = require("../response");
var account = require("../lib/account");
var middlewarePermission = require("../middleware/checkPermission");

module.exports = function (app, io) {
  app.route("/accounts").post([middlewarePermission], (req, res) => {
    account
      .create(req.user, req.body)
      .then((ok) => {
        resp.sendOK(res, req, ok);
      })
      .catch(function (err) {
        resp.throws(res, req, err);
      });
  }),
    app.route("/accounts/login/staff").post((req, res) => {
      account
        .loginStaff(req.body, io)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch(function (err) {
          resp.throws(res, req, err);
        });
    }),
    app.route("/accounts/login/customer").post((req, res) => {
      account
        .loginCustomer(req.body, io)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch(function (err) {
          resp.throws(res, req, err);
        });
    }),
    app
      .route("/accounts/:account_Id")
      .delete([middlewarePermission], (req, res) => {
        account
          .delete(req.params.account_Id, req.user, req.body)
          .then((ok) => {
            resp.sendOK(res, req, ok);
          })
          .catch(function (err) {
            resp.throws(res, req, err);
          });
      }),
    app.route("/listaccounts").post([middlewarePermission], (req, res) => {
      account
        .list(req.user, req.body, req.body)
        .then((ok) => {
          resp.sendOK(res, req, ok);
        })
        .catch(function (err) {
          resp.throws(res, req, err);
        });
    });
  app.route("/accounts/logined").get([middlewarePermission], (req, res) => {
    account
      .logined(req.user)
      .then((ok) => {
        resp.sendOK(res, req, ok);
      })
      .catch(function (err) {
        resp.throws(res, req, err);
      });
  });

  app.route("/accounts-create-dev").post((req, res) => {
    account
      .createdev(req.body)
      .then((ok) => {
        resp.sendOK(res, req, ok);
      })
      .catch(function (err) {
        resp.throws(res, req, err);
      });
  });

  app.route("/accounts/:account_Id").get([middlewarePermission], (req, res) => {
    account
      .findbycode(req.params.account_Id, req.user)
      .then((ok) => {
        resp.sendOK(res, req, ok);
      })
      .catch(function (err) {
        resp.throws(res, req, err);
      });
  });

  app.route("/accounts/:account_Id").put([middlewarePermission], (req, res) => {
    account
      .update(req.params.account_Id, req.user, req.body)
      .then((ok) => {
        resp.sendOK(res, req, ok);
      })
      .catch(function (err) {
        resp.throws(res, req, err);
      });
  });
};
