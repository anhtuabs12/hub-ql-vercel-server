"use strict";

var resp = require("../response");
var logs = require("../lib/logs");
var middlewarePermission = require("../middleware/checkPermission");
const io = require("../../server");

module.exports = function (app, passport) {
  app.route("/logs").post([middlewarePermission], (req, res) => {
    logs
      .list(req.user, req.body )
      .then((ok) => {
        resp.sendOK(res, req, ok);
      })
      .catch(function (err) {
        resp.throws(res, req, err);
      });
  });
};
