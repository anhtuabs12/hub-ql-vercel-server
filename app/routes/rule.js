"use strict";

var resp = require("../response");
var rule = require("../lib/rule");
var middlewarePermission = require("../middleware/checkPermission");

module.exports = function (app, passport) {
    app.route("/rule").post([middlewarePermission],(req, res) => {
        rule
        .create(req.user, req.body)
        .then((ok)=>{
            resp.sendOK(res, req, ok);
        })
        .catch(function(err){
            resp.throws(res, req, err);
        })
    });

    app.route("/rule/:rule_code").put([middlewarePermission],(req,res)=>{
        rule
        .update(req.params.rule_code, req.user, req.body)
        .then((ok)=>{
            resp.sendOK(res, req, ok);
        })
        .catch(function(err){
            resp.throws(res,req,err);
        })
    });

    app.route("/rule/:rule_code").delete([middlewarePermission], (req, res)=>{
        rule
        .delete(req.params.rule_code, req.user, req.body)
        .then((ok)=>{
            resp.sendOK(res,req,ok);
        })
        .catch(function(err){
            resp.throws(res, req, err);
        })
    });

    app.route("/rule").get([middlewarePermission],(req, res)=>{
        rule
        .list(req.user, req.body, req.query)
        .then((ok)=>{
            resp.sendOK(res,req,ok);
        })
        .catch(function(err){
            resp.throws(res,req,err);
        })
    });

    app.route("/rulebycode/:rule_code").get([middlewarePermission],(req,res)=>{
        rule
        .findbycode(req.params.rule_code)
        .then((ok)=>{
            resp.sendOK(res,req,ok);
        })
        .catch((err)=>{
            resp.throws(res,req,err);
        })
    });

    
}