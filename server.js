require('dotenv').config({
    path: '.env'
});

var fs = require('fs');

// var options = {
//     key: fs.readFileSync('/var/local/ssl/care123.vn.key'),
//     cert: fs.readFileSync('/var/local/ssl/ssl-bundle.crt'),
// }
var express = require('express');
var app = express();
var server = require('http').createServer(app);
// var server = require('https').createServer(options,app);
var port = process.env.port_server || 3001;

var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cors = require('cors');
var device = require('express-device');
var useragent = require('express-useragent');
var path = require('path');


const { Server } = require("socket.io");

const io = new Server(server, {
    cors: {
      origin: true,
      credentials: true,
    },
    allowEIO3: true,
  });

io.on('connection', (socket) => {
    console.log('a user connected');

    // Xử lý sự kiện disconnect
    socket.on('disconnect', () => {
        console.log('A client disconnected');
    });

    // Nhận tin nhắn từ client
    socket.on('sendMessage', (message) => {
        console.log('Received message from client:', message);
    });
  });


app.use(bodyParser.json());
app.use(bodyParser.json({type: 'application/*+json'}))
app.use(bodyParser.urlencoded({extended: true}));
app.use(device.capture());
app.use(useragent.express());
app.use(cors());
app.use(methodOverride('X-HTTP-Method-Override'));

app.use('/uploads',express.static(path.join(__dirname, 'uploads')));

device.enableViewRouting(app);

var middleware = require('./app/middleware');
var routes = require('./app/routes');

console.log(routes ,"routes");
// var cron = require('./app/cron');
app.get("/", express.static( "./uploads"));
// Định tuyến ======================================================================
middleware(app);
try {
    routes(app, io);
} catch (err) {
    console.log(err);
}

const schedule = require('node-schedule');

// let scheduleTime =  '*/5 * * * * *' 

// const job = schedule.scheduleJob(scheduleTime, function(){
//   scheduleTime = '* * * * * *'
//   console.log('The answer to life, the universe, and everything!' , scheduleTime);
// });

//Chạy server
server.listen(port);
console.log('Server is running on ' + port);
exports = module.exports = app;
//}

