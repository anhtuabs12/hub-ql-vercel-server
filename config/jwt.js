module.exports = {
  secret_token: process.env.secret_token || "3csoft-fucking",
  password_default: "123456",
  max_user: process.env.max_user || 10,
  elasticsearch_url: process.env.elasticsearch_url || "http://localhost:9200/",
  roles: {
    ADMINISTRATOR: "administrator",
    SALEADMIN: "saleadmin",
    CUSTOMER: "customer",
  },

  logType: {
    CREATE: "create",
    UPDATE: "update",
  },

  BrandLogsType: {
    HOTLINE: "hotline",
    ACCOUNT: "account",
    BRANDNAME: "brandname",
    TICKET: "ticket",
  },
  logTypeKey: {
    STATUS_KEEPING: "status_keeping",
    STATUS_EVICTING: "status_evicting",
    STATUS_CANCELING: "status_canceling",
    STATUS_READY: "status_ready",
    STATUS_DOING: "status_doing",
    STATUS_WAITINGBRANDNAME: "status_waitingbrandname",
  },

  hotlineStatus: {
    READY: "ready", // chưa sử dụng
    KEEPING: "keeping", // giữ số
    WAITBRANDNAME: "waitbrandname", // chờ gắn name
    DOING: "doing", // đang sử dụng
    OFF: "off", // số đã bị hủy
    EVICT: "evict", // số đang bị thu hồi
  },

  ruleType: {
    MINIMUM_NUMBER_HOTLINES: "Minimum",
    MAXIMUM_HOTLINES_KEEP_BY_CUSTOMER: "maximum",
    WITHDRAWAL: 1,
    CANCEL_NUMBER: 2,
    UNUSED: 3,
  },

  ticketType: {
    WITHDRAWAL: 1,
    CANCEL_NUMBER: 2,
    UNUSED: 3,
  },

  statusType: {
    WAITING: 1,
    AGREE: 2,
    DISAGREE: 3,
  },

  TicketFromType: {
    STAFF: 1,
    CUSTOMER: 2,
  },

  statusBrandnameType: {
    Pause: 1,
    Activate: 2,
  },

  netWorkId: {
    VIETTEL: "1",
    VINA: "2",
    MOBI: "3",
  },

  typelogs: {
    HOTLINE: "hotline",
    ACCOUNT: "account",
    CUSTOMER: "customer",
  },
  actions: {
    CREATED_USER: "created-user",
    LOGIN_USER: "logined-user",
    // RESET_PASS : 'reset-pass',
    DELETE_USER: "delete-user",

    CREATED_CUSTOMER: "created-customer",
    DELETE_CUSTOMER: "delete-customer",
    UPDATED_CUSTOMER: "updated-customer",

    CREATED_HOTLINE: "created-hotline",
    DELETE_HOTLINE: "delete-hotline",
    UPDATED_HOTLINE: "updated-hotline",

    // CREATED_COMMENT : 'created-comment',
    // CREATED_TICKET: 'created-ticket',
    // UPDATED_TICKET : 'updated-ticket',
    // CREATED_CAMPAIGN: 'created-campaign',
    // UPDATED_CAMPAIGN: 'updated-campaign',
    // DELETED_CAMPAIGN: 'deleted-campaign',
    // CREATE_BLACKLIST: 'create-blacklist',
    // UPDATE_BLACKLIST: 'update-blacklist',
    // DELETE_BLACKLIST: 'delete-blacklist'
  },

  socketType: {
    CHECK_LOGINED: "socket-login-token",
    REQEST_RELOAD_HOTLINE: "request-reload-hotline",
  },

  email_status: {
    delete: 333,
    draft: 102,
    sent: 201,
    new: 0,
  },
};
